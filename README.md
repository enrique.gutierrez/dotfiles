# Dotfiles

This repository contains configurations scripts that are to be shared accross Linux systems

# Setup

The following instructions are for Linux distributions with the `apt` package manager (Ubuntu, Debian, etc.)

## Requirements

1. Git*
```bash
sudo apt-get install git
```
2. Curl*
```
sudo apt-get install curl
```
3. Ansible
```bash
sudo apt-get install ansible
```    

*: Should be included in most distributions by default

## Install

Pull this repo via Ansible
```bash
sudo ansible-pull -U <REPO-URL.git>
```

For a specific branch of this repo use:
```bash
sudo ansible-pull -U <REPO-URL.git> -C <BRANCH>
```