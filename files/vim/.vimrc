"---------------------------------------------------------------
" 		                   OPTIONS
"---------------------------------------------------------------
set nocompatible    " Choose no compatibility with legacy vi
set wildmode=longest,list,full
set tabstop=4
set shiftwidth=4    " Indents will have a width of 4
set softtabstop=4   " Sets the number of columns for a TAB
set expandtab       " Expand TABs to spaces
set backspace=indent,eol,start  " backspace through everything in insert mode
map q: <Nop>        " Disable EX mode shortcut
nnoremap Q <Nop>    " Disable EX mode shortcut

"---------------------------------------------------------------
"                          APPEARANCE
"---------------------------------------------------------------
set ruler   "Show the cursor position at all time
color darkblue
syntax enable
filetype plugin on
filetype indent on
set number relativenumber
set showcmd     " SHow partial commands below the status line
set scrolloff=8   "Always have lines before window botton
set titlestring=%f title    " Display filename in terminal window 
set splitbelow splitright   " When split go below or right first
set wrap            " Wrap text on terminal width
set linebreak       " Break the word and not the character
set nolist         " List disables linebreak

"---------------------------------------------------------------
"                          SEARCH
"---------------------------------------------------------------
set hlsearch
" set ignorecase
set smartcase
set enc=utf-8

"---------------------------------------------------------------
"                            GUI
"---------------------------------------------------------------
" set mousemodel=extend " Enable mouse support
" set selectmode=mouse
" set mousefocus
" set mouse=a

"---------------------------------------------------------------
"                          COOL STUFF
"---------------------------------------------------------------
" <Tab> indents if at the beginning of a line; otherwise does completion
function! InsertTabWrapper()
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    else
        return "\<c-n>"
    endif
endfunction
inoremap <tab> <c-r>=InsertTabWrapper()<cr>
inoremap <s-tab> <c-p>

" disable cursor keys in normal mode
" map <Left>  :echo "no!"<cr>
" map <Right> :echo "no!"<cr>
" map <Up>    :echo "no!"<cr>
" map <Down>  :echo "no!"<cr>

" yank to system clipboard
map <leader>y "*y

" WSL yank support
let s:clip = '/mnt/c/Windows/System32/clip.exe'  " change this path according to your mount point
if executable(s:clip)
    augroup WSLYank
        autocmd!
        autocmd TextYankPost * if v:event.operator ==# 'y' | call system(s:clip, @0) | endif
    augroup END
endif


"---------------------------------------------------------------
"                          VUNDLE
"---------------------------------------------------------------
set shell=/bin/bash " To ensure compatibility with Vundle
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

Plugin 'VundleVim/Vundle.vim'        " let Vundle manage Vundle
Plugin 'scrooloose/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'airblade/vim-gitgutter'
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-commentary'
Plugin 'ReplaceWithRegister'
Plugin 'christoomey/vim-system-copy'
Plugin 'junegunn/fzf.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
