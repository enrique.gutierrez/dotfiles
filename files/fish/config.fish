#!/usr/bin/fish

# For communicating with Xserver in Windows
# set -x DISPLAY localhost:0    # WSL1
set -x DISPLAY (grep -oP "(?<=nameserver ).+" /etc/resolv.conf):0.0    # WSL2 - Test using `xev` command
alias xsel="xsel | clip.exe"

# Add local/bin to PATH
set PATH $HOME/.local/bin $PATH

# Abbreviations
abbr la "ls -la"
abbr ll "ls -alF" 
abbr la "ls -A" 
abbr jn "jupyter notebook --no-browser"
abbr r "ranger --choosedir='$HOME/.rangerdir'; cd (cat $HOME/.rangerdir)"
abbr ip "ifconfig eth0 | rg '(.+inet )(\d*.\d*.\d*.\d*).+' -r '\$2'"
abbr ga "git add"
abbr gb "git branch"
abbr gc "git checkout"
abbr gcb "git checkout -b"
abbr gco "git checkout origin"
abbr gm "git commit -m"
abbr gd "git diff -p"
abbr gdi "git difftool -t image_diff"
abbr gs "git status --short"
abbr gh "git stash"
abbr gf "git fetch"
abbr gr "git rm -r --cached"
abbr gl "git log --oneline --abbrev-commit --all --graph --decorate --color"
abbr gi "vim .git/info/exclude" # git ignore
abbr week "date +%U"
abbr neo "neofetch --ascii_distro windows10"
abbr f "find | rg -e"
abbr slideshow "feh -z -D 5 --recursive --auto-zoom --geometry 1920x1080"
abbr sc "screen"
abbr scl "screen -ls"
abbr scr "screen -r"
abbr ch "curl cheat.sh/"

# Fish prompt visuals
function fish_prompt
    # Virtual environment
    if not set -q VIRTUAL_ENV_DISABLE_PROMPT
        set -g VIRTUAL_ENV_DISABLE_PROMPT true
    end
    # Full length current directory
    set -g fish_prompt_pwd_dir_length 80
    # Line 1
    set_color red 
    printf '%s' (whoami)
    set_color normal
    printf ' at ' 
    set_color blue
    printf '%s'(hostname)
    set_color normal
    printf ' in ' 
    set_color yellow
    printf '%s' (prompt_pwd)
    set_color normal
    printf ':'
    # Line 2
    echo
    if test $VIRTUAL_ENV
        set_color white
       printf "(%s) " (basename $VIRTUAL_ENV)
       set_color normal
       end
       printf '>>> '
     set_color normal
 end


function fish_right_prompt 
    set -l last_status $status
    set -g __fish_git_prompt_show_informative_status 1
    set -g __fish_git_prompt_hide_untrackedfiles 1

    set -g __fish_git_prompt_showupstream "informative" # verbose name informative git svn none
    set -g __fish_git_prompt_char_stateseparator ""        # "|"
    set -g __fish_git_prompt_char_upstream_prefix ""    # ""
    set -g __fish_git_prompt_char_upstream_ahead "»"    # "↑"
    set -g __fish_git_prompt_char_upstream_behind "«"   # "↓"
    set -g __fish_git_prompt_char_stagedstate " ∫"       # "●"
    set -g __fish_git_prompt_char_dirtystate " ∂"        # "✚"
    set -g __fish_git_prompt_char_untrackedfiles "+"    # "…"
    set -g __fish_git_prompt_char_conflictedstate "x"   # "✖"
    set -g __fish_git_prompt_char_upstream_diverged "!"
    set -g __fish_git_prompt_char_upstream_equal "="
    set -g __fish_git_prompt_char_cleanstate "@"        # "✔"
    set -g __fish_git_prompt_char_stashstate "#"        # "↩"

    set -g __fish_git_prompt_color_branch blue
    set -g __fish_git_prompt_color_dirtystate magenta
    set -g __fish_git_prompt_color_stagedstate yellow
    set -g __fish_git_prompt_color_invalidstate red
    set -g __fish_git_prompt_color_cleanstate green
    set -g __fish_git_prompt_color_upstream_ahead green
    set -g __fish_git_prompt_color_upstream_behind red
    set -g __fish_git_prompt_color_untrackedfiles $fish_color_normal

    # Actual printing
    printf '%s ' (__fish_vcs_prompt)
    date '+%y-%m-%d %H:%M:%S ['
    math (date +%U) + 1 # Week number should start on 1
    printf ']'
end
